<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <div class="content-wrapper">
                <section class="content-header">
                    <?php echo $pagetitle; ?>
                    <?php echo $breadcrumb; ?>
                </section>

                <section class="content">
                    <?php echo $dashboard_alert_file_install; ?>
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-aqua"><i class="fa fa-legal"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Agenda Masuk</span>
                                    <span class="info-box-number"><?php echo $count_agenda?></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-green"><i class="fa fa-check"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Agenda selesai</span>
                                    <span class="info-box-number"><?php echo $count_semua_agenda_selesai ?></span>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix visible-sm-block"></div>

                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-red"><i class="fa fa-exclamation-triangle"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">agenda tidak selesai</span>
                                    <span class="info-box-number"><?php echo $count_semua_agenda_tidakselesai?></span>
                                </div>
                            </div>
                        </div>
                       
                    </div>

                    <div class="row">
                        <div class="col-md-12">
<?php
/*
if ($url_exist) {
    echo 'OK';
} else {
    echo 'KO';
}
*/
?>
                        </div>

                        <div class="col-md-12">
                            <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">!!! Perhatian Agenda Tanpa Kegiatan Terhitung sebagai agenda yang tidak terselesaikan</h3>
                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                    </div>
                                </div>
                                <div class="box-body">

                                  <div class="dataTable_wrapper">
			<div class="table-responsive">
			<table class="table table-striped table-bordered table-hover">
			<thead>
			<tr>
				<th class="col-md-1"><center>#</center></th>
				<th class="col-md-2">NIP/Nama</th>
				<th class="col-md-3">tgl</th>
				<th class="col-md-2">ket_agenda</th>
				<th class="col-md-1">jumlah_kegiatan</th>
				<th class="col-md-1">belum_approve</th>
				<th class="col-md-1">sudah_approve</th>
			</tr>
			</thead>
			<tbody>
	    	<?php
	    	if($allagenda){
	    		$i=1;
				foreach ( $allagenda as $row ) {
			
                    if($row->jumlah_kegiatan == $row->sudah_approve && $row->jumlah_kegiatan > 0 ){
                          $warna = "bg-green";  
                    }elseif($row->jumlah_kegiatan > $row->sudah_approve){
                        $warna = "bg-red";  
                  
                    }else{
                          $warna = "";
                    }
                ?>

		        <tr class="gradeX <?=$warna?>">
		        <td><?php echo $i?></td>
		        <td><?php echo $row->nip." / ".$row->nama?></td>
		        <td><?php echo $row->tgl?></td>
		        <td><?php echo $row->ket_agenda?></td>
		        <td><?php echo $row->jumlah_kegiatan?></td>
		        <td><?php echo $row->belum_approve?></td>
		        <td><?php echo $row->sudah_approve?></td>
		       
				</tr>
	        <?php 
	        	$i++;
				} 
	    	}else{
	        	echo "<tr class=\"gradeX\"><td colspan='4'>No Record</td></tr>";
	        }?>
	    	</tbody>
			</table>
			</div>
			<!-- <div align="right"><?php echo $links?> </div> -->
		</div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </section>
            </div>
