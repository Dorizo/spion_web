<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Database extends Admin_Controller {

   


	public function index()
	{
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            
            /* Load Template */
            $this->template->admin_render('admin/approve/index', $this->data);
        }
	}
}
