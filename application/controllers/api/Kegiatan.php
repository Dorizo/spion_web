<?php

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
class Kegiatan extends REST_Controller {
    function __construct()
	{
    parent::__construct();
    $this->load->library('ion_auth');
    $this->load->model ('setup/api_model');
    $this->load->helper('string');
    $this->load->helper('path');
    $this->load->helper(array('form', 'url'));
    }

  function index_post()
  {




    $config['upload_path']          = './upload/kegiatan/';
    $config['allowed_types']        = 'gif|jpg|png';
    $config['max_size']             = 100;
    $config['max_width']            = 1024;
    $config['max_height']           = 768;
    $config['encrypt_name']           = TRUE;

    $this->load->library('upload', $config);

    if ( ! $this->upload->do_upload('image'))
                {
                        $error = array('error' => $this->upload->display_errors());
                        if(!empty($this->post("id_users")) && !empty($this->post("detail"))&& !empty($this->post("lokasi"))&& !empty($this->post("koordinat"))){
                              $this->response($this->api_model->create($this->post() , "default.png") , 201);
                           }else{
                             $this->response(array("message" => $this->upload->display_errors()) , 400);
                           } 


                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());
                           if(!empty($this->post("id_users")) && !empty($this->post("detail"))&& !empty($this->post("lokasi"))&& !empty($this->post("koordinat"))){
                          $this->response($this->api_model->create($this->post() ,$data['upload_data']['file_name']) , 201);
                       }else{
                         $this->response(array("message" => "gagal di input") , 400);
                       }
                }



    // $encoded_string = $this->post("image_kegiatan");
    // $img = explode(',', $encoded_string);
    // if(count($img) > 1 ){
    //   $ini =substr($img[0], 11);
    //   $type = explode(';', $ini);
    //   if($type[0] == 'jpeg'){
    //     $types = 'jpg';
    //   }else{
    //     $types = $type[0];
    //   }
    //   $uploadname =random_string('alnum', 16).".".$types;
    //   $temp_file_path = tempnam(sys_get_temp_dir(), 'androidtempimage'); // might not work on some systems, specify your temp path if system temp dir is not 
    //   $path = set_realpath('upload/kegiatan/');
    //   $data = base64_decode($img[1]);
    //   file_put_contents($path . $uploadname, $data);
    //   getimagesize($path . $uploadname);
    //   // $this->response(array("response" => $types ,"responsew" => getimagesize(set_realpath('upload/kegiatan/').$uploadname) , "dir" => $uploadname));
    //   if(!empty($this->post("id_users")) && !empty($this->post("detail"))&& !empty($this->post("lokasi"))&& !empty($this->post("koordinat"))){
    //     $this->response($this->api_model->create($this->post() , $uploadname) , 201);
    //  }else{
    //    $this->response(array("message" => "gagal di input") , 400);
    //  }
    
    // }else{
    //   if(!empty($this->post("id_users")) && !empty($this->post("detail"))&& !empty($this->post("lokasi"))&& !empty($this->post("koordinat"))){
    //     $this->response($this->api_model->create($this->post() , "default.png") , 201);
    //  }else{
    //    $this->response(array("message" => "gagal di input") , 400);
    //  }
    // }


     
  }

  function kegiatanagenda_get(){
    
    if(!empty($this->get("id_users")) && !empty($this->get("limit"))){
      $this->response(array("response" =>$this->api_model->kegiatanagenda($this->get())->result()) , 200);
      }else{
       $this->response(array("response" => "parometer yang di masukan ada yang kurang") , 400);
      };
  
  }

  function kegiatansaya_get(){
    if(!empty($this->get("id_users")) && !empty($this->get("limit"))){
    $this->response(array("response" =>$this->api_model->kegiatansaya($this->get())->result()) , 200);
    }else{
     $this->response(array("response" => "parometer yang di masukan ada yang kurang") , 400);
    };
  }
  function kegiatansemua_get(){
    if(!empty($this->get("limit"))){
    $this->response(array("response" =>$this->api_model->kegiatansemua($this->get())->result()) , 200);
    }else{
     $this->response(array("response" => "parometer yang di masukan ada yang kurang") , 400);
    };
  }



  function solokegiatan_get(){
    if(!empty($this->get('id_kegiatan'))){
      $this->response(array("response" => $this->api_model->solokegiatan($this->get())->row()) ,200);
    }else{
      $this->response(array("response" => "parometer yang di masukan ada yang kurang") , 400);
     };
  }

  function update_post(){
    if(!empty($this->post("id_kegiatan")) && !empty($this->post("id_users")) && !empty($this->post("detail"))&& !empty($this->post("lokasi"))&& !empty($this->post("koordinat"))){
      $this->response($this->api_model->updatekegiatan($this->post()) , 201);
   }else{
     $this->response(array("message" => "gagal di input") , 400);
   }
  }

  function parentview_get(){
    
    if(!empty($this->get("parent_position")) && !empty($this->get("tgl"))){
      $this->response(array("response" =>$this->api_model->parentviewkegiatan_get($this->get())->result()) , 200);
    }else{
     $this->response(array("response" => "Agenda Aprove kosong" ), 400);   
    };

  } 

  function approve_post(){
    if(!empty($this->post('approve')) && !empty($this->post('id_kegiatan'))){
      $this->response(array("response" => $this->api_model->approve($this->post())) ,200);
    }else{
      $this->response(array("response" => "parometer yang di masukan ada yang kurang") , 400);
     };
  }



}

?>
