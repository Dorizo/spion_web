<?php

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
class Login extends REST_Controller {


  function __construct()
	{
    parent::__construct();
    $this->load->library('ion_auth');
    $this->load->model ('setup/pegawai_model' );
    $this->load->model ('setup/position_model' );
    $this->load->model ('setup/groups_model' );
  }

  function index_post()
  {
    if ($this->ion_auth->login($this->post('username'), $this->post('password'), FALSE))
    {
      $user = $this->ion_auth->user()->row();
      $groups = $this->ion_auth->get_users_groups($user->id)->row();
      $pegawai = $this->pegawai_model->fetchById_api($user->nip)[0];
      $data = array('status_login' => "berhasil" , 'message' => 'Login Berhasil');
      $this->response(array('login'=>array($data),'data'=>array($pegawai)));
    }else{
      $data = array('status_login' => "gagal" , 'message' => 'Login Anda gagal mohon di periksa kembali' );
      $this->response(array('login'=>array($data),'data'=>array(null)));
    }
  }

    function user_get()
    {
        $this->response($data);
    }

    function user_post()
    {
        $this->response($data);
    }

    function user_put()
    {
        $this->response($data);
    }

    function user_delete()
    {
        $this->response($data);
    }




  }


?>
