<?php

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
class Agendaakhir extends REST_Controller {
    function __construct()
	{
    parent::__construct();
    $this->load->library('ion_auth');
    $this->load->model ('setup/api_agendaakhir');
    $this->load->helper('string');
    $this->load->helper('path');
    $this->load->helper(array('form', 'url'));
    }

  function index_post()
  {


    $config['upload_path']          = './upload/kegiatan/';
    $config['allowed_types']        = 'gif|jpg|png';
    $config['max_size']             = 100;
    $config['max_width']            = 1024;
    $config['max_height']           = 768;
    $config['encrypt_name']           = TRUE;

    $this->load->library('upload', $config);

    if ( ! $this->upload->do_upload('image'))
                {

    if(!empty($this->post("id_users")) && !empty($this->post("ket_agenda"))&& !empty($this->post("id_posisi"))&& !empty($this->post("waktu"))){
               $this->response($this->api_agendaakhir->createAgenda($this->post() , "default.png") , 201);
      }else{
        $this->response(array("message" => "gagal di input") , 400);
      }

    }
    else
    {


      $data = array('upload_data' => $this->upload->data());
      if(!empty($this->post("id_users")) && !empty($this->post("ket_agenda"))&& !empty($this->post("id_posisi"))&& !empty($this->post("waktu"))){
        $this->response($this->api_agendaakhir->createAgenda($this->post(),$data['upload_data']['file_name']) , 201);
      }else{
      $this->response(array("message" => "gagal di input") , 400);
      }

    }
  
    }

    function updates_post()
    {


      $config['upload_path']          = './upload/kegiatan/';
      $config['allowed_types']        = 'gif|jpg|png';
      $config['max_size']             = 100;
      $config['max_width']            = 1024;
      $config['max_height']           = 768;
      $config['encrypt_name']           = TRUE;
  
      $this->load->library('upload', $config);
  
      if ( ! $this->upload->do_upload('image'))
                  {
    
        
      if(!empty($this->post("id_users")) && !empty($this->post("ket_agenda")) && !empty($this->post("id_posisi"))&& !empty($this->post("waktu"))){
                 $this->response($this->api_agendaakhir->updateAgenda($this->post(), "default.png") , 201);
        }else{
          $this->response(array("message" => "gagal di input") , 400);
        }
      }
      else
      {

       $data = array('upload_data' => $this->upload->data());
        if(!empty($this->post("id_users")) && !empty($this->post("ket_agenda")) && !empty($this->post("id_posisi"))&& !empty($this->post("waktu"))){
          $this->response($this->api_agendaakhir->updateAgenda($this->post(),$data['upload_data']['file_name']) , 201);
        }else{
          $this->response(array("message" => "gagal di input") , 400);
        
        }
      }
    
      }



      function approve_post()
      {
    
          
                   $this->response($this->api_agendaakhir->approveagendawal($this->post()) , 201);
       
        }

      function agendasaya_get(){
        if(!empty($this->get("id_users")) && !empty($this->get("limit"))){
        $this->response(array("response" =>$this->api_agendaakhir->agendasaya($this->get())->result()) , 200);
        }else{
         $this->response(array("response" => "parometer yang di masukan ada yang kurang") , 400);   
        };
          
      }
    

  function agendaharian_get(){
    if(!empty($this->get("id_users")) && !empty($this->get("tgl"))){
      $this->response(array("response" =>$this->api_agendaakhir->agendaharian($this->get())->result()) , 200);
    }else{
     $this->response(array("response" => "Agenda tanggal "+$this->get("tgl")+" Kosong" , 400));   
    };
   
  }



  

  
  function hapus_delete(){
    $a =  $this->delete("id");
    $delcon = $this->api_agendaakhir->hapus($a);
    if($delcon == TRUE){
      $this->response(array("response" => "Berhasil di hapus") , 200);  
    }else{
      $this->response(array("response" => "sudah pernah di hapus") , 400);  
    };

  }



  function agendasemua_get(){
    if(!empty($this->get("tgl")) && !empty($this->get("limit"))){
    $this->response(array("response" =>$this->api_agendaakhir->agendasemua($this->get())->result()) , 200);
    }else{
     $this->response(array("response" => "parometer yang di masukan ada yang kurang") , 400);   
    };
      
  }
  function parentview_get(){
    
    if(!empty($this->get("parent_position")) && !empty($this->get("tgl"))){
      $this->response(array("response" =>$this->api_agendaakhir->parentview_get($this->get())->result()) , 200);
    }else{
     $this->response(array("response" => "Agenda Aprove kosong" ), 400);   
    };

  }



}

?>