<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class kegiatan_model extends CI_Model {
	function __construct() {
		parent::__construct ();
	}
	
	public function record_count() {
		$this->db->where('tgl_kegiatan',date('Y-m-d'));
		return $this->db->count_all("kegiatan_tbl");
	}

//SELECT a.* , (select count(*) FROM kegiatan WHERE id_agenda=a.id_agenda AND mode_approve=1) AS belum_approve , (select count(*) FROM kegiatan WHERE id_agenda=a.id_agenda AND mode_approve=2) AS sudah_approve FROM `agenda` as a GROUP BY a.id_agenda ORDER BY a.`id_users` ASC
	public function fetchAllByNipmember($nip,$tgl) {
		$this->db->select ('a.* ,p.* , (select count(*) FROM kegiatan WHERE id_agenda=a.id_agenda) as jumlah_kegiatan ,(select count(*) FROM kegiatan WHERE id_agenda=a.id_agenda AND mode_approve=1) AS belum_approve , (select count(*) FROM kegiatan WHERE id_agenda=a.id_agenda AND mode_approve=2) AS sudah_approve');
		$this->db->from ('agenda as a');
		$this->db->join("pegawai_tbl p" , "p.nip = a.id_users");
		$this->db->where('id_users',$nip);
		$query = $this->db->get ();
		if ($query->num_rows()> 0) {
			foreach ( $query->result () as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}

	public function allagenda() {
		$this->db->select ('a.* ,p.* , (select count(*) FROM kegiatan WHERE id_agenda=a.id_agenda) as jumlah_kegiatan ,(select count(*) FROM kegiatan WHERE id_agenda=a.id_agenda AND mode_approve=1) AS belum_approve , (select count(*) FROM kegiatan WHERE id_agenda=a.id_agenda AND mode_approve=2) AS sudah_approve');
		$this->db->from ('agenda as a');
		$this->db->join("pegawai_tbl p" , "p.nip = a.id_users");
		$query = $this->db->get ();
		if ($query->num_rows()> 0) {
			foreach ( $query->result () as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}




	//parometer untuk agenda dengan kegiatan terselesaikan atau tidak
	//SELECT a.* , (select count(*) FROM kegiatan WHERE id_agenda=a.id_agenda AND mode_approve=1) AS belum_approve , (select count(*) FROM kegiatan WHERE id_agenda=a.id_agenda AND mode_approve=2) AS sudah_approve , (select count(*) FROM kegiatan WHERE id_agenda=a.id_agenda) AS totalagenda  FROM `agenda` as a  
    //WHERE (select count(*) FROM kegiatan WHERE id_agenda=a.id_agenda) = (select count(*) FROM kegiatan WHERE id_agenda=a.id_agenda AND mode_approve=2) and (select count(*) FROM kegiatan WHERE id_agenda=a.id_agenda) > 0
    //GROUP BY a.id_agenda ORDER BY a.`id_users` ASC

	public function count_semua_agenda_selesai(){
		$this->db->select ('a.* , (select count(*) FROM kegiatan WHERE id_agenda=a.id_agenda AND mode_approve=1) AS belum_approve , (select count(*) FROM kegiatan WHERE id_agenda=a.id_agenda AND mode_approve=2) AS sudah_approve , (select count(*) FROM kegiatan WHERE id_agenda=a.id_agenda) AS totalagenda');
		$this->db->from ('agenda as a');
		$this->db->where('(select count(*) FROM kegiatan WHERE id_agenda=a.id_agenda) = (select count(*) FROM kegiatan WHERE id_agenda=a.id_agenda AND mode_approve=2)');
		$this->db->where('(select count(*) FROM kegiatan WHERE id_agenda=a.id_agenda) > 0');
		return $this->db->count_all_results();
	}

	public function count_semua_agenda_tidakselesai(){
		$this->db->select ('a.* , (select count(*) FROM kegiatan WHERE id_agenda=a.id_agenda AND mode_approve=1) AS belum_approve , (select count(*) FROM kegiatan WHERE id_agenda=a.id_agenda AND mode_approve=2) AS sudah_approve , (select count(*) FROM kegiatan WHERE id_agenda=a.id_agenda) AS totalagenda');
		$this->db->from ('agenda as a');
		$this->db->where('(select count(*) FROM kegiatan WHERE id_agenda=a.id_agenda) <= (select count(*) FROM kegiatan WHERE id_agenda=a.id_agenda AND mode_approve=1)');
		// $this->db->where('(select count(*) FROM kegiatan WHERE id_agenda=a.id_agenda) > 0');
		return $this->db->count_all_results();
	}



	
	public function fetchAllByNip($nip,$limit, $start) {
		$this->db->select ('*');
		$this->db->from ('kegiatan_tbl');
		$this->db->where('nip',$nip);
		$this->db->limit ($limit, $start);
		$query = $this->db->get ();
		if ($query->num_rows()> 0) {
			foreach ( $query->result () as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function fetchById($nip,$kd_skpd,$kd_position,$kd_kegiatan, $tgl_kegiatan){
		$this->db->select ('*');
		$this->db->from ('kegiatan_tbl');
		$this->db->where('nip',$nip);
		$this->db->where('kd_skpd',$kd_skpd);
		$this->db->where('kd_position',$kd_position);
		$this->db->where('kd_kegiatan',$kd_kegiatan);
		$this->db->where('tgl_kegiatan',$tgl_kegiatan);
		$query = $this->db->get()->result_array();
		return $query;
	}
	
	public function fetchByDate($nip,$kd_skpd,$kd_position,$tgl_kegiatan){
		$this->db->select ('*');
		$this->db->from ('kegiatan_tbl');
		$this->db->where('nip',$nip);
		$this->db->where('kd_skpd',$kd_skpd);
		$this->db->where('kd_position',$kd_position);
		$this->db->where('tgl_kegiatan',$tgl_kegiatan);
		$query = $this->db->get()->result_array();
		return $query;
	}
	
	public function generateKegiatan($nip){
		$this->db->select('IFNULL(MAX(kd_kegiatan),0)+1 id');
		$this->db->from('kegiatan_tbl');
		$this->db->where('nip',$nip);
		$this->db->where('tgl_kegiatan',date('Y-m-d'));
		$query= $this->db->get();
		$ret = $query->row();
		return $ret->id;
	}
	
	public function create($data) {
		$this->id_users = $data['id_users'];
		$this->koordinat = $data['koordinat'];
		$this->tgl = $data['tgl'];
		$this->jam = $data['jam'];
		$this->detail = $data['detail'];
		$this->id_agenda = $data['id_agenda'];
		$this->mode_approve = $data['mode_approve'];
		$this->lokasi = $data['lokasi'];
		
		// insert data
		$this->db->insert('kegiatan', $this);
	}

	public function createAgenda($data) {
		// $this->id_kegiatan = $data['id_kegiatan'];
		$this->tgl = $data['tgl'];
		$this->waktu = $data['waktu'];
		$this->ket_agenda = $data['ket_agenda'];	
		$this->id_posisi = $data['id_posisi'];
		$this->id_users = $data['id_users'];		
		if($this->db->insert('agenda', $this)){
			return array("message" => "Data Berhasil Di input");
		}else{
			$data = $this->db->error();
			return array("message" => "Data Gagal Di input");
		}
	}


	public function update($data) {
		// get data
		$this->nip = $data['nip'];
		$this->kd_skpd = $data['kd_skpd'];
		$this->kd_position = $data['kd_position'];
		$this->kd_tupoksi = $data['kd_tupoksi'];
		$this->kd_uraian = $data['kd_uraian'];
		$this->kd_kegiatan = $data['kd_kegiatan'];
		$this->tgl_kegiatan = $data['tgl_kegiatan'];
		$this->uraian_kegiatan = $data['uraian_kegiatan'];
		
		// update data
		$this->db->update ('kegiatan_tbl', $this, array ('nip'=> $this->nip = $data['nip'], 'kd_skpd' => $data['kd_skpd'], 'kd_position'=>$data['kd_position'], 'kd_kegiatan'=>$data['kd_kegiatan'], 'tgl_kegiatan'=>$data['tgl_kegiatan']));
	}
	
	public function delete($id) {
		if($this->db->delete ('kegiatan_tbl', array ('qid' => $id))){
			return "Data succesfully deleted!";
		}else{
			$data = $this->db->error();
			return $data['message'];
		}
	}
	
	public function search_count($column, $data){
		$this->db->where($column,$data);
		return  $this->db->count_all('kegiatan_tbl');
	}
	
	public function search($column,$value, $limit, $start){
		
		$this->db->select ('*');
		$this->db->from ('kegiatan_tbl');
		$this->db->like($column,$value);
		$this->db->limit ($limit, $start);
		$query = $this->db->get();
		if ($query->num_rows()> 0) {
			foreach ( $query->result() as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}
	
}