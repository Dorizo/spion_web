<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class groups_model extends CI_Model {

	public function getall() {
		$this->db->select ('*');
		$this->db->from ('groups');
		$this->db->order_by('id' ,'DESC');
		$query = $this->db->get ();
		if ($query->num_rows()> 0) {
			foreach ( $query->result () as $row ) {
				$data[$row->id] = $row->name;
			}
			return $data;
		}
		return false;
	}

	public function getskpd() {
		$this->db->select ('*');
		$this->db->from ('skpd_tbl');
		$query = $this->db->get ();
		if ($query->num_rows()> 0) {
			foreach ( $query->result () as $row ) {
				$data[$row->kd_skpd] = $row->nama;
			}
			return $data;
		}
		return false;
	}

	public function getposisi() {
		$this->db->select ('*');
		$this->db->from ('position_tbl');
		$query = $this->db->get ();
		if ($query->num_rows()> 0) {
			foreach ( $query->result () as $row ) {
				$data[$row->kd_position] = $row->nama;
			}
			return $data;
		}
		return false;
	}
	
}
?>