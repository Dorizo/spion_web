<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class api_model extends CI_Model {
	function __construct() {
		parent::__construct ();
		$this->load->model('admin/user_operation_model');
		$this->load->model('setup/position_model');
	}



	public function fetchById($id){
		$this->db->select ('p.nip, ifnull(p.nip_lama,"-") nip_lama, p.nama, p.picture, p.gelar_depan, p.gelar_belakang,
		p.tempat_lahir, p.tgl_lahir, p.telp, p.kelamin , p.agama, p.gol_darah, p.status,  p.no_kartu_pegawai, p.no_askes, p.no_taspen, p.no_kartu_keluarga, p.npwp, p.qversion, p.qid,p.alamat, p.kode_pos, p.kd_position');
		$this->db->from ('pegawai_tbl p');
		$this->db->where('p.nip',$id);
		$query = $this->db->get()->result_array();
		return $query;
	}

	public function createAgenda($data) {
		// $this->id_kegiatan = $data['id_kegiatan'];
		$this->tgl = $data['tgl'];
		$this->waktu = $data['waktu'];
		$this->ket_agenda = $data['ket_agenda'];
		$this->id_posisi = $data['id_posisi'];
		$this->id_users = $data['id_users'];
		if($this->db->insert('agenda', $this)){
			return array("message" => "Data Berhasil Di input");
		}else{
			$data = $this->db->error();
			return array("message" => "Data Gagal Di input");
		}
	}

	public function updateAgenda($data) {
		$this->tgl = $data['tgl'];
		$this->waktu = $data['waktu'];
		$this->ket_agenda = $data['ket_agenda'];
		$this->id_posisi = $data['id_posisi'];
		$this->id_users = $data['id_users'];
		$this->db->where('id_agenda' ,$data['id_agenda']);
		if($this->db->update('agenda', $this)){
			return array("message" => "Data Berhasil Di input");
		}else{
			// $data = $this->db->error();
			return array("message" => "Data Gagal Di input");
		}
	}

	public function agendasaya($data) {
		$this->db->select ('p.* , (select count(*) FROM kegiatan WHERE id_agenda=p.id_agenda) as jumlah_kegiatan ,(select count(*) FROM kegiatan WHERE id_agenda=p.id_agenda AND mode_approve=1) AS belum_approve , (select count(*) FROM kegiatan WHERE id_agenda=p.id_agenda AND mode_approve=2) AS sudah_approve');
		$this->db->from ('agenda p');
		$this->db->where('id_users' , $data['id_users']);
		$this->db->limit($data['limit'] , $data['mulai'] );
		$this->db->order_by("id_agenda" , "DESC");
		return $query = $this->db->get();

	}


	public function agendasemua($data) {
		$this->db->select ('p.* , po.nama as jabatan ,COALESCE(keg.mode_approve , 1) as mode_approve ,  (select count(*) FROM kegiatan WHERE id_agenda=p.id_agenda) as jumlah_kegiatan ,(select count(*) FROM kegiatan WHERE id_agenda=p.id_agenda AND mode_approve=1) AS belum_approve , (select count(*) FROM kegiatan WHERE id_agenda=p.id_agenda AND mode_approve=2) AS sudah_approve');
		$this->db->from ('agenda p');

		// 
		$this->db->join('kegiatan keg' , 'keg.id_agenda = p.id_agenda' ,'LEFT');
		// $this->db->where('po.parent_position' , $data['parent_position']);
		$this->db->where('p.tgl' , $data['tgl']);
		$this->db->join('pegawai_tbl peg' , 'peg.nip = p.id_users');
		$this->db->join("position_tbl po" , "po.kd_position = p.id_posisi");
		$this->db->limit($data['limit'] , $data['mulai'] );
		$this->db->order_by("id_agenda" , "DESC");

	}

	public function parentview_get($data) {
		$this->db->select ('p.* , peg.* ,COALESCE(keg.mode_approve , 1) as mode_approve , (select count(*) FROM kegiatan WHERE id_agenda=p.id_agenda) as jumlah_kegiatan ,(select count(*) FROM kegiatan WHERE id_agenda=p.id_agenda AND mode_approve=1) AS belum_approve , (select count(*) FROM kegiatan WHERE id_agenda=p.id_agenda AND mode_approve=2) AS sudah_approve');
		$this->db->from ('agenda p');
		// ,COALESCE(keg.mode_approve , 1) as mode_approve
		$this->db->join('kegiatan keg' , 'keg.id_agenda = p.id_agenda' ,'LEFT');
		$this->db->where('po.parent_position' , $data['parent_position']);
		$this->db->where('p.tgl' , $data['tgl']);
		$this->db->join('pegawai_tbl peg' , 'peg.nip = p.id_users');
		$this->db->join("position_tbl po" , "po.kd_position = p.id_posisi");
		$this->db->limit($data['limit'] , $data['mulai'] );
		$this->db->order_by("p.id_agenda" , "DESC");
		return $query = $this->db->get();

	}

	public function parentviewkegiatan_get($data) {


		$this->db->select ('k.*,p.*,peg.* , po.nama as jabatan');
		$this->db->from ('kegiatan k');
		$this->db->where('po.parent_position' , $data['parent_position']);
		$this->db->where('p.tgl' , $data['tgl']);
		$this->db->join('agenda p' , 'p.id_agenda = k.id_agenda' );
		$this->db->join('pegawai_tbl peg' , 'peg.nip = p.id_users');
		$this->db->join("position_tbl po" , "po.kd_position = p.id_posisi");
		$this->db->limit($data['limit'] , $data['mulai'] );
		$this->db->order_by("k.id_agenda" , "DESC");
		return $query = $this->db->get();


	}

	public function agendaharian($data) {
		$this->db->select ('p.*,peg.* ,COALESCE(keg.mode_approve , 1) as mode_approve , po.nama as jabatan , (select count(*) FROM kegiatan WHERE id_agenda=p.id_agenda) as jumlah_kegiatan ,(select count(*) FROM kegiatan WHERE id_agenda=p.id_agenda AND mode_approve=1) AS belum_approve , (select count(*) FROM kegiatan WHERE id_agenda=p.id_agenda AND mode_approve=2) AS sudah_approve');
		$this->db->from ('agenda p');
		// ,COALESCE(keg.mode_approve , 1)
		$this->db->join('kegiatan keg' , 'keg.id_agenda = p.id_agenda' ,'LEFT');
		$this->db->join('pegawai_tbl peg' , 'peg.nip = p.id_users');
		$this->db->join("position_tbl po" , "po.kd_position = p.id_posisi");
		$this->db->where('p.id_users' , $data['id_users']);
		$this->db->where('p.tgl' , $data['tgl']);
		$this->db->limit($data['limit'] , $data['mulai'] );
		$this->db->order_by("p.id_agenda" , "DESC");
		return $query = $this->db->get();

	}



	public function hapus($id){
		$this->db->where('id_agenda', $id);
		$this->db->delete('agenda');
		$del = $this->db->affected_rows();
		return $del;
	}


	public function create($data , $foto) {

		

		// $this->id_kegiatan = $data['id_kegiatan'];
		$this->tgl = $data['tgl'];
		$this->jam = $data['jam'];
		$this->koordinat = $data['koordinat'];
		$this->lokasi = $data['lokasi'];
		$this->detail = $data['detail'];
		$this->id_users = $data['id_users'];
		$this->mode_approve = $data['mode_approve'];
		$this->id_agenda = $data['id_agenda'];
		$this->id_agenda = $data['id_agenda'];
		$this->foto = $foto;
		if($this->db->insert('kegiatan', $this)){
			return array("message" => "Data Berhasil Di input");
		}else{
			$data = $this->db->error();
			return array("message" => "Data Gagal Di input");
		}
	}



	public function kegiatansaya($data) {
		$this->db->select ('p.*,peg.* , po.nama as jabatan');
		$this->db->from ('kegiatan p');
		$this->db->join('pegawai_tbl peg' , 'peg.nip = p.id_users');
		$this->db->join("position_tbl po" , "po.kd_position = peg.kd_position");
		$this->db->where('id_users' , $data['id_users']);
		$this->db->where('tgl' , $data['tgl']);
		$this->db->limit($data['limit'] , $data['mulai'] );
		$this->db->order_by("id_kegiatan" , "DESC");
		return $query = $this->db->get();
	}


	public function kegiatanagenda($data) {
		$this->db->select ('p.*,peg.* , po.nama as jabatan');
		$this->db->from ('kegiatan p');
		$this->db->join('pegawai_tbl peg' , 'peg.nip = p.id_users');
		$this->db->join("position_tbl po" , "po.kd_position = peg.kd_position");
		$this->db->where('p.id_agenda' , $data['id_users']);
		$this->db->where('tgl' , $data['tgl']);
		$this->db->limit($data['limit'] , $data['mulai'] );
		$this->db->order_by("id_kegiatan" , "DESC");
		return $query = $this->db->get();
	}

	public function kegiatansemua($data) {
		$this->db->select ('p.*,peg.* , po.nama as jabatan , po.kd_group');
		$this->db->from ('kegiatan p');
		$this->db->join('pegawai_tbl peg' , 'peg.nip = p.id_users');
		$this->db->join("position_tbl po" , "po.kd_position = peg.kd_position");
		$this->db->where('tgl' , $data['tgl']);
		$this->db->limit($data['limit'] , $data['mulai'] );
		$this->db->order_by("id_kegiatan" , "DESC");
		return $query = $this->db->get();
	}


	public function solokegiatan($data) {
		$this->db->select ('*');
		$this->db->from ('kegiatan p');
		$this->db->where('id_kegiatan' , $data['id_kegiatan']);
		$this->db->limit(1);
		$this->db->order_by("id_kegiatan" , "DESC");
		return $query = $this->db->get();

	}

	public function updatekegiatan($data){
		$this->tgl = $data['tgl'];
		$this->jam = $data['jam'];
		$this->koordinat = $data['koordinat'];
		$this->lokasi = $data['lokasi'];
		$this->detail = $data['detail'];
		$this->id_users = $data['id_users'];
		$this->id_agenda = $data['id_agenda'];
		$this->db->where('id_kegiatan' ,$data['id_kegiatan']);
		if($this->db->update('kegiatan', $this)){
			return array("message" => "Data Berhasil Di Update");
		}else{
			$data = $this->db->error();
			return array("message" => "Data Gagal Di update");
		}
	}
	public function approve($data){
		$this->mode_approve = $data['approve'];
		$this->db->where('id_kegiatan' ,$data['id_kegiatan']);
		if($this->db->update('kegiatan', $this)){
			return array("message" => "Data Berhasil Di Update");
		}else{
			$data = $this->db->error();
			return array("message" => "Data Gagal Di update");
		}
	}





}
