<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );
class api_agendaakhir extends CI_Model {
	function __construct() {
		parent::__construct ();
		$this->load->model('admin/user_operation_model');
		$this->load->model('setup/position_model');
    }
    
	public function createAgenda($data , $namafoto) {
		// $this->id_kegiatan = $data['id_kegiatan'];
		$this->tgl = $data['tgl'];
		$this->waktu = $data['waktu'];
		$this->ket_agenda = $data['ket_agenda'];
		$this->id_posisi = $data['id_posisi'];
		$this->id_users = $data['id_users'];
		$this->kegiatan1 = $data['kegiatan1'];
		$this->kegiatan2 = $data['kegiatan2'];
		$this->foto = $namafoto;
		$this->kordinat = $data['kordinat'];
		$this->mode_approve = $data['mode_approve'];
		if($this->db->insert('agendaakhir', $this)){
			return array("message" => "Data Berhasil Di input");
		}else{
			$data = $this->db->error();
			return array("message" => "Data Gagal Di input");
		}
    }
    

	public function updateAgenda($data, $namafoto) {
		$this->tgl = $data['tgl'];
		$this->waktu = $data['waktu'];
		$this->ket_agenda = $data['ket_agenda'];
		$this->id_posisi = $data['id_posisi'];
		$this->id_users = $data['id_users'];
		$this->kegiatan1 = $data['kegiatan1'];
		$this->kegiatan2 = $data['kegiatan2'];
		$this->foto = $namafoto;
		$this->kordinat = $data['kordinat'];
		$this->mode_approve = $data['mode_approve'];
		$this->db->where('id_agenda' ,$data['id_agenda']);
		if($this->db->update('agendaakhir', $this)){
			return array("message" => "Data Berhasil Di input");
		}else{
			// $data = $this->db->error();
			return array("message" => "Data Gagal Di input");
		}
	}

	public function approveagendawal($data) {
		$this->mode_approve = $data['mode_approve'];
		$this->db->where('id_agenda' ,$data['id_agenda']);
		if($this->db->update('agendaakhir', $this)){
			return array("message" => "Data Berhasil Di input");
		}else{
			// $data = $this->db->error();
			return array("message" => "Data Gagal Di input");
		}
	}


	public function agendasaya($data) {
		$this->db->select ('p.* , (select count(*) FROM kegiatan WHERE id_agenda=p.id_agenda) as jumlah_kegiatan ,(select count(*) FROM kegiatan WHERE id_agenda=p.id_agenda AND mode_approve=1) AS belum_approve , (select count(*) FROM kegiatan WHERE id_agenda=p.id_agenda AND mode_approve=2) AS sudah_approve');
		$this->db->from ('agendaakhir p');
		$this->db->where('id_users' , $data['id_users']);
		$this->db->limit($data['limit'] , $data['mulai'] );
		$this->db->order_by("id_agenda" , "DESC");
		return $query = $this->db->get();

    }
    
    public function agendaharian($data) {
		$this->db->select ('p.*,peg.* , po.nama as jabatan , (select count(*) FROM kegiatan WHERE id_agenda=p.id_agenda) as jumlah_kegiatan ,(select count(*) FROM kegiatan WHERE id_agenda=p.id_agenda AND mode_approve=1) AS belum_approve , (select count(*) FROM kegiatan WHERE id_agenda=p.id_agenda AND mode_approve=2) AS sudah_approve');
		$this->db->from ('agendaakhir p');
		// ,COALESCE(keg.mode_approve , 1)
		$this->db->join('pegawai_tbl peg' , 'peg.nip = p.id_users');
		$this->db->join("position_tbl po" , "po.kd_position = p.id_posisi");
		$this->db->where('p.id_users' , $data['id_users']);
		$this->db->where('p.tgl' , $data['tgl']);
		$this->db->limit($data['limit'] , $data['mulai'] );
		$this->db->order_by("p.id_agenda" , "DESC");
		return $query = $this->db->get();

	}


	public function agendasemua($data) {
		$this->db->select ('p.* , po.nama as jabatan ,  (select count(*) FROM kegiatan WHERE id_agenda=p.id_agenda) as jumlah_kegiatan ,(select count(*) FROM kegiatan WHERE id_agenda=p.id_agenda AND mode_approve=1) AS belum_approve , (select count(*) FROM kegiatan WHERE id_agenda=p.id_agenda AND mode_approve=2) AS sudah_approve');
		$this->db->from ('agendaakhir p');
		// $this->db->where('po.parent_position' , $data['parent_position']);
		$this->db->where('p.tgl' , $data['tgl']);
		$this->db->join('pegawai_tbl peg' , 'peg.nip = p.id_users');
		$this->db->join("position_tbl po" , "po.kd_position = p.id_posisi");
		$this->db->limit($data['limit'] , $data['mulai'] );
		$this->db->order_by("id_agenda" , "DESC");
		return $query = $this->db->get();

	}

	public function parentview_get($data) {
		$this->db->select ('p.* , peg.*  , (select count(*) FROM kegiatan WHERE id_agenda=p.id_agenda) as jumlah_kegiatan ,(select count(*) FROM kegiatan WHERE id_agenda=p.id_agenda AND mode_approve=1) AS belum_approve , (select count(*) FROM kegiatan WHERE id_agenda=p.id_agenda AND mode_approve=2) AS sudah_approve');
		$this->db->from ('agendaakhir p');
		// ,COALESCE(keg.mode_approve , 1) as mode_approve
		$this->db->where('po.parent_position' , $data['parent_position']);
		$this->db->where('p.tgl' , $data['tgl']);
		$this->db->join('pegawai_tbl peg' , 'peg.nip = p.id_users');
		$this->db->join("position_tbl po" , "po.kd_position = p.id_posisi");
		$this->db->limit($data['limit'] , $data['mulai'] );
		$this->db->order_by("p.id_agenda" , "DESC");
		return $query = $this->db->get();

	}

}

?>