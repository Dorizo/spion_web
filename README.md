# CI-AdminLTE v1.1.4

## Demo

Coming soon

### Login
 * Email : `admin@admin.com`
 * Password : `password`

# Fungsi Agenda CRUD

## Create Agenda kegiatan 

[create agenda per user](http://http://localhost/spion/api/agenda)
http://localhost/spion/api/agenda [POST]

###method post yang di buat dibawah ini 
* id_users:     2
* tgl:          2019-05-18
* waktu:10:35:  13
* ket_agenda:   Keterangan agenda
* id_posisi:    KDJ002


## update agenda Agenda kegiatan 

http://localhost/spion/api/Agenda/updates [POST]

###method post yang di buat dibawah ini 
* id_users:     2
* tgl:          2019-05-18
* waktu:10:35:  13
* ket_agenda:   Keterangan agenda
* id_posisi:    KDJ002
* id_agenda:1



## fungsi view agenda Per user dengan  parometer GET :
[create agenda per user](http://localhost/spion/api/agenda/agendasaya?id_users=2&limit=5&mulai=0)
http://localhost/spion/api/agenda/agendasaya
### parometer yang di get 
id_users:2
limit:5
mulai:0


## fungsi view agenda semua user dengan  parometer GET :
[create agenda per user](http://localhost/spion/api/agenda/agendasaya?id_users=2&limit=5&mulai=0)
http://localhost/spion/api/agenda/agendasemua
### parometer yang di get 
limit:5
mulai:0
tgl : tgl




## fingsi view agenda harian 
* http://localhost/spion/api/agenda/agendaharian

### parometer agenda harian  GET
* id_users:2
* limit:5
* mulai:0
* tgl:2019-05-18



## fingsi view agenda harian 
* http://localhost/spion/api/agenda/parentview

### parometer agenda harian  GET
* parent_position:KDJ001
* limit:5
* mulai:0
* tgl:2019-05-18

### code 

* OkHttpClient client = new OkHttpClient();

* Request request = new Request.Builder()
*   .url("http://localhost/spion/api/agenda/parentview?parent_position=KDJ001&limit=5&mulai=0&tgl=2019-05-18")
*   .get()
*   .addHeader("Content-Type", "application/x-www-form-urlencoded")
*   .addHeader("Authorization", "Basic YWRtaW46MTIzNA==")
*   .addHeader("Cache-Control", "no-cache")
*   .addHeader("Postman-Token", "77e03634-31e3-44eb-b462-23948932950b")
*   .build();

* Response response = client.newCall(request).execute();








## fungsi hapus agenda

* OkHttpClient client = new OkHttpClient();

* MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
* RequestBody body = RequestBody.create(mediaType, "id=9");
* Request request = new Request.Builder()
 *  .url("http://localhost/spion/api/agenda/hapus")
 *  .delete(body)
 *  .addHeader("Content-Type", "application/x-www-form-urlencoded")
 *  .addHeader("Authorization", "Basic YWRtaW46MTIzNA==")
 *  .addHeader("Cache-Control", "no-cache")
 *  .addHeader("Postman-Token", "cda7e490-9164-4740-8166-04d6bae4dda8")
 *  .build();

Response response = client.newCall(request).execute();

### parometer DELETE
* id



## method create fungsi kegiatan 
[url login post](http://http://localhost/spion/api/kegiatan/format/json)
http://localhost/spion/api/kegiatan/format/json

###method post yang di buat dibawah ini 
* tgl:2019-05-18
* jam:10:35:13
* koordinat: -2000202,30039994 
* lokasi:lampung
* detail:ssss
* id_users:2
* mode_approve:1
* id_agenda:3

## fungsi view kegiatan saya / penginput
* url
* http://localhost/spion/api/kegiatan/kegiatansaya

### parometer yang di panggil
* id_users (id user yang membuat data) int
* limit (limit data yang di panggil) int 
* mulai (start / mulai pada baris) int





## fungsi view kegiatanagenda  / penginput
* url
*http://localhost/spion/api/kegiatan/kegiatanagenda/format/json?

### parometer yang di panggil
* id_users (id user yang membuat data) int
* limit (limit data yang di panggil) int 
* mulai (start / mulai pada baris) int
* tgl=2019-05-18


## fungsi view kegiatan semua agenda pertanggal
* url
* http://localhost/spion/api/kegiatan/kegiatansemua

### parometer yang di panggil
* tgl:2019-05-18
* limit (limit data yang di panggil) int 
* mulai (start / mulai pada baris) int



## kegiatan solo dengan id kegiatan untuk edit 
* url http://localhost/spion/api/kegiatan/solokegiatan

### parameter get 
* id_kegiatan=2


## update kegiatan dengan id_kegiatan sebagai parometer
* url http://localhost/spion/api/kegiatan/solokegiatan
### parameter update  

* id_kegiatan:8
* tgl:2019-05-18
* jam:10:35:13
* koordinat: -2000202,30039994 
* lokasi:update bisa ngak 
* detail:update berhasil
* id_users:2
* mode_approve:1
* id_agenda:1


## Approve post kegiatan POST
* url http://localhost/spion/api/kegiatan/approve
### parameter update  
* id_kegiatan:2
* approve:3



# agenda awal ada disini 

## membuat agenda awal POST
http://localhost/spion/api/Agendaawal
### parameter update 
* id_users:2
* tgl:2019-05-18
* waktu:10:35:13
* ket_agenda: Keterangan agenda
* id_posisi:KDJ002
* mode_approve:1
* kegiatan1: ss
* kegiatan2: sss

## update agenda awal POST

http://localhost/spion/api/Agendaawal/updates
### parrometer update

* id_agenda:2
* id_users:8111111
* tgl:2019-05-18
* waktu:10:35:13
* ket_agenda:asuuu
* id_posisi:KDJ002
* kegiatan1: Keterangan agenda
* kegiatan2: Keterangan agenda
* mode_approve:2

## agenda awal saya 

http://localhost/spion/api/agendaawal/agendasaya

### parrometer update 

* id_users:8111111
* limit:10
* mulai:0

## agenda awal harian saya

http://localhost/spion/api/agendaawal/agendaharian

### paroteter update 

* id_users:08111111
* limit:5
* mulai:0
* tgl:2019-05-18

### agenda awal semua 

http://localhost/spion/api/agendaawal/agendasemua

### paroteter update 

* tgl:2019-05-18
* limit:10
* mulai:0

## view agenda atasan masing masing

http://localhost/spion/api/agendaawal/parentview

### update paromteter

* parent_position:KDJ001
* limit:5
* mulai:0
* tgl:2019-05-18


## approve agenda awal

http://localhost/spion/api/Agendaawal/approve

### update paromater


* id_agenda:2
* mode_approve:3







# agenda akhir ada disini 

## membuat agenda akhir POST
http://localhost/spion/api/Agendaakhir
### parameter update 
* id_users:2
* tgl:2019-05-18
* waktu:10:35:13
* ket_agenda: Keterangan agenda
* id_posisi:KDJ002
* mode_approve:1
* kegiatan1: ss
* kegiatan2: sss
* kordinat:-222222,39939.666979797
* image : 

## update agenda akhir POST

http://localhost/spion/api/Agendaakhir/updates
### parrometer update

* id_agenda:2
* id_users:8111111
* tgl:2019-05-18
* waktu:10:35:13
* ket_agenda:asuuu
* id_posisi:KDJ002
* kegiatan1: Keterangan agenda
* kegiatan2: Keterangan agenda
* mode_approve:2
* kordinat:-222222,39939.666979797
* image : 

## agenda akhir saya 

http://localhost/spion/api/Agendaakhir/agendasaya

### parrometer update 

* id_users:8111111
* limit:10
* mulai:0

## agenda akhir harian saya

http://localhost/spion/api/Agendaakhir/agendaharian

### paroteter update 

* id_users:08111111
* limit:5
* mulai:0
* tgl:2019-05-18

### agenda akhir semua 

http://localhost/spion/api/Agendaakhir/agendasemua

### paroteter update 

* tgl:2019-05-18
* limit:10
* mulai:0

## view agenda atasan masing masing

http://localhost/spion/api/Agendaakhir/parentview

### update paromteter

* parent_position:KDJ001
* limit:5
* mulai:0
* tgl:2019-05-18


## approve agenda akhir

http://localhost/spion/api/Agendaakhir/approve

### update paromater


* id_agenda:2
* mode_approve:3



























## Dependencies
| NAME | VERSION | WEB | REPO |
| :--- | :---: | :---: | :---: |
| CodeIgniter | 3.0.4 | [Website](http://codeigniter.com) | [Github](https://github.com/bcit-ci/CodeIgniter/)
| AdminLTE | 2.3.2 | [Website](https://almsaeedstudio.com) | [Github](https://github.com/almasaeed2010/AdminLTE/)
| Bootstrap | 3.3.6 | [Website](http://getbootstrap.com) | [Github](https://github.com/twbs/bootstrap)
| Ion Auth | 2.6.0 | [Website](http://benedmunds.com/ion_auth) | [Github](https://github.com/benedmunds/CodeIgniter-Ion-Auth)
| jQuery | 2.2.0 | [Website](http://jquery.com) | [Github](https://github.com/jquery/jquery)
| Font Awesome | 4.5.0 | [Website](http://fortawesome.github.io/Font-Awesome/) | [Github](https://github.com/FortAwesome/Font-Awesome) 